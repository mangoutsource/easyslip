��          �       L      L  (   M     v     �     �     �     �     �     �          
       	   5  w   ?     �     �  W   �     $     *  �  3  �   �  3   X  N   �     �  $   �  3        P  *   l     �  '   �  7   �  3   �  5  3  $   i  -   �  �   �  !   �	     �	   After submission, change order status to Confirm Payment Confirm payment successfully EasySlip Bank Account Name EasySlip Bank Account Number EasySlip Image EasySlip Status Invalid License Mark as verified No. %s. payment total: %s Order No. Payment Notification

Name: %s
Order ID: %s
Bank: %s
Account Number: %s
Amount: %s
Date: %s

Order Details:
%s
Link: %s Payment Slip Recheck Show form on Thank you page. shortcode: [easyslip id="<order id>"] where id is optional Valid status:  Project-Id-Version: EasySlip
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-11-17 10:59+0000
PO-Revision-Date: 2024-01-08 04:04+0000
Last-Translator: 
Language-Team: Thai
Language: th
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.6; wp-6.4.1
X-Domain: easyslip-main หลังจากลูกค้าอัปโหลดสลิป อยากให้สถานะเปลี่ยนเป็น ยืนยันการชำระเงิน ยืนยันการชำระเงินเรียบร้อย ชื่อบัญชี หมายเลขบัญชี หลักฐานการโอนเงิน สถานะสลิป สลิปไม่ถูกต้อง License ยืนยันถูกต้อง เลขที่: %s - จำนวนเงิน %s หมายเลขคำสั่งซื้อ แจ้งชำระเงิน

ชื่อ: %s
เลขที่คำสั่งซื้อ: %s
ธนาคาร: %s
เลขที่บัญชี: %s
จำนวนเงิน: %s
วันที่: %s

รายละเอียดคำสั่งซื้อ:
%s
ลิงก์: %s สลิปจ่ายเงิน ตรวจสอบอีกครั้ง แสดงฟอร์มแจ้งชำระเงินในหน้าขอบคุณ. สามารถกำหนด shortcode: [easyslip id="<order id>"] โดยที่ id ระบุหรือไม่ระบุก็ได้ สลิปถูกต้อง สถานะ:  
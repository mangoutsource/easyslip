<?php 

namespace Helpers;

class URLHelper {
/**
 * The function adds a query parameter to the current URL.
 * 
 * @return the updated URL with the added query parameter.
 */
  public static function addQuery($name, $value) {
    $currentUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if (strpos($currentUrl, '?') !== false) {
      return $currentUrl . "&$name=$value";
    } 

    return $currentUrl . "?$name=$value";
  }

  /**
   * The function checks if a given value is present in the current request URI.
   * 
   * @param value The value parameter is the string that you want to check if it exists within the
   * REQUEST_URI of the  superglobal variable.
   * 
   * @return the position of the first occurrence of the value within the REQUEST_URI string.
   */
  public static function contains($value) {
    return strpos($_SERVER['REQUEST_URI'], $value);
  }

  /**
   * Get the value of a specific query parameter from the current URL.
   * 
   * @param name The name of the query parameter.
   * 
   * @return The value of the query parameter, or null if the parameter is not present.
   */
  public static function getQuery($name) {
    $queryString = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
    parse_str($queryString, $params);

    return isset($params[$name]) ? $params[$name] : null;
  }

  /**
   * The function removes a query parameter from the current URL.
   * 
   * @param name The name of the query parameter to be removed.
   * 
   * @return The updated URL with the specified query parameter removed.
   */
  public static function removeQuery($name) {
    $currentUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $urlParts = parse_url($currentUrl);

    if (isset($urlParts['query'])) {
      parse_str($urlParts['query'], $params);
      unset($params[$name]);

      $queryString = http_build_query($params);
      $updatedUrl = $urlParts['path'] . '?' . $queryString;
    } else {
      $updatedUrl = $currentUrl;
    }

    return $updatedUrl;
  }
}
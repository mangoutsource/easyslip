<?php
/*
Plugin Name: EasySlip
Description: EasySlip and WordPress seamless integration
Version: 1.0.11
Author: EasySlip
Author URI: https://easyslip.com
Plugin URI: https://easyslip.com
*/

define('EASYSLIP_VERSION', '1.0.11');
define('EASYSLIP_SLUG', 'easyslip-main');

if (!defined('Carbon_Fields\URL')) {
  define('Carbon_Fields\URL', trailingslashit( plugin_dir_url( __FILE__ ) ) . 'vendor/htmlburger/carbon-fields/' );
}

require 'vendor/autoload.php';

load_plugin_textdomain('easyslip-main', false, dirname(plugin_basename(__FILE__)) . '/languages/');

use Clients\AdminClient;
use Clients\HookClient;
use Clients\ShortcodeClient;
use Clients\UpdaterClient;

new AdminClient();
new HookClient();
new ShortcodeClient();
new UpdaterClient();

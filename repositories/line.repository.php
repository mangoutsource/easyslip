<?php

namespace Repositories;

class LineNotifyRepository {
  private $apiUrl;

  /**
   * The function is a constructor that initializes the API URL and access token for a Line Notify
   * object.
   * 
   * @param accessToken The `accessToken` parameter is a string that represents the access token
   * required to authenticate and authorize the API requests to the LINE Notify service. This access
   * token is obtained by registering your application with LINE Notify and generating an access token
   * for it.
   */
  public function __construct() {
    $this->apiUrl = 'https://notify-api.line.me/api/notify';
  }

  /**
   * The function `sendOrderSlipVerificationNotification` sends a notification with order details and a
   * payment slip image to a specified line token.
   * 
   * @param order The "order" parameter is an object that represents an order. It contains information
   * about the order such as the customer's name, billing address, order items, total amount, etc.
   */
  public function sendOrderSlipVerificationNotification($order, $slipURL) {
    $orderId = $order->get_id();
    $bankName = carbon_get_post_meta($orderId, 'easyslip_bank_name');
    $bankAccountNumber = carbon_get_post_meta($orderId, 'easyslip_bank_account_number');
    $fullname = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
    $date = date_i18n('d-m-Y H:i');
    $link = admin_url('post.php?post=' . $orderId . '&action=edit');

    $orderItems = '';

    foreach ($order->get_items() as $item_id => $item) {
      $product_name = $item->get_name();
      $quantity = $item->get_quantity();
      $total = $order->get_line_total($item, true, true);
      $orderItems .= sprintf("%s x %d = %s\n", $product_name, $quantity, $total);
    }

    $message = __("Payment Notification\n\nName: %s\nOrder ID: %s\nBank: %s\nAccount Number: %s\nAmount: %s\nDate: %s\n\nOrder Details:\n%s\nLink: %s", 'easyslip-main');
    $message = sprintf(
        $message,
        $fullname,
        $orderId,
        $bankName,
        $bankAccountNumber,
        $order->get_total(),
        $date,
        $orderItems,
        $link
    );

    $this->send($message, $slipURL);
  }

  /**
   * The function `sendNotification` sends a POST request to an API with a message and returns the HTTP
   * status code and response.
   * 
   * @param message The "message" parameter is the notification message that you want to send. It can
   * be a string containing the content of the notification.
   * 
   * @return an array with two keys: 'status' and 'response'. The 'status' key contains the HTTP status
   * code of the request, and the 'response' key contains the response from the API, parsed as a JSON
   * object.
   */
  public function send($message, $imageURL) {
    $token = carbon_get_theme_option('easyslip_line_token');
    if (!$token) return;

    $curl = curl_init();

    $data = ['message' => $message];
    
    if ($imageURL) {
      $data['imageThumbnail'] = $imageURL;
      $data['imageFullsize'] = $imageURL;
    }

    curl_setopt_array($curl, [
      CURLOPT_URL => $this->apiUrl,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => http_build_query($data),
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/x-www-form-urlencoded',
        "Authorization: Bearer {$token}"
      ],
    ]);

    $response = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    return [
      'status' => $httpCode,
      'response' => json_decode($response, true)
    ];
  }
}

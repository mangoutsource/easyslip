<?php 

namespace Repositories;

class CarbonRepository {

  /**
   * The saveField function saves a key-value pair as post meta using the carbon_set_post_meta function.
   * 
   * @param postId The postId parameter is the ID of the post where the field will be saved.
   * @param key The "key" parameter is a string that represents the name or identifier of the field you
   * want to save. It is used to uniquely identify the field when retrieving or updating its value.
   * @param value The value is the data that you want to save for the specified key. It can be any type
   * of data, such as a string, number, array, or object.
   */
  public function saveField($postId, $key, $value) {
    carbon_set_post_meta($postId, $key, $value);
  }

  /**
   * The getField function retrieves a specific field value from the post meta data using the
   * carbon_get_post_meta function.
   * 
   * @param postId The postId parameter is the ID of the post for which you want to retrieve the meta
   * field value. It is typically an integer value that uniquely identifies a post in the WordPress
   * database.
   * @param key The "key" parameter is a string that represents the name of the custom field you want to
   * retrieve the value for. It is used to identify the specific field you want to get the value from.
   * 
   * @return the value of the post meta field with the specified key for the given post ID.
   */
  public function getField($postId, $key) {
    return carbon_get_post_meta($postId, $key);
  }

  /**
   * The function `getOption` retrieves a value from the theme options using the
   * `carbon_get_theme_option` function.
   * 
   * @param key The key parameter is a string that represents the name of the option you want to retrieve
   * from the theme options.
   * 
   * @return the value of the theme option with the specified key.
   */
  public function getOption($key) {
    return carbon_get_theme_option($key);
  }
}
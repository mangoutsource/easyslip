<?php 

namespace Repositories;

class WordPressRepository {

  /**
   * The function retrieves all meta values from the postmeta table in the WordPress database that match
   * a given meta key.
   * 
   * @param key The key parameter is the meta key for which you want to retrieve the meta values.
   * 
   * @return an array of meta values from the postmeta table in the WordPress database, where the
   * meta_key matches the provided key.
   */
  public function getPostMetaValuesFromMetaKeyWithoutPostId($key, $ignoreIds) {
    global $wpdb;

    $ignoreIds = implode(',', array_map('intval', $ignoreIds));
    $query = "SELECT meta_value FROM {$wpdb->postmeta} WHERE meta_key = \"$key\" AND post_id NOT IN ($ignoreIds)";
    $values = $wpdb->get_results($query, ARRAY_A);
    return array_column($values, 'meta_value');
  }
}
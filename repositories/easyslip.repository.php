<?php

namespace Repositories;

class EasySlipRepository {
  private $apiUrl;
  private $apiKey;

  /**
   * The function is a constructor that initializes the apiUrl and apiKey properties of an object.
   * 
   * @param apiUrl The `apiUrl` parameter is the URL of the API that you want to connect to. It is the
   * base URL that will be used to make API requests.
   * @param apiKey The apiKey parameter is a string that represents the API key used for authentication
   * and authorization purposes. It is typically provided by the API provider and is required to access
   * the API endpoints.
   */
  public function __construct() {
    $this->apiUrl = 'https://developer.easyslip.com';
  }

  /**
   * The function `verifyPayload` sends a request to an API endpoint with a payload and returns the
   * response if the HTTP status code is 200, otherwise it returns an error message.
   * 
   * @param payload The `payload` parameter is a string that represents the data you want to verify. It
   * could be any type of data, such as a JSON object or a plain text string. The purpose of the
   * `verifyPayload` function is to send this payload to an API endpoint for verification.
   * 
   * @return an array. If the HTTP response code is 200, it will return the decoded JSON response as an
   * associative array. If the HTTP response code is not 200, it will return an array with an 'error'
   * key and the value 'Failed to verify payload'.
   */
  public function verifyPayload($image, $targetTotal, $targetBankAccountName, $targetBankNumber, $existingPayloads) {
    if (!$image) return;

    $file = $this->getAbsoluteImagePath($image);

    $key = carbon_get_theme_option('easyslip_license');

    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => $this->apiUrl . '/api/v1/verify',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTPHEADER => ["Authorization: Bearer $key"],
      CURLOPT_POSTFIELDS => ['file' => new \CURLFile($file)],
    ]);

    $response = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($response);

    if ($result->status == 200) {
      return [
        "status" => $this->checkMatch($result, $targetTotal, $targetBankAccountName, $targetBankNumber, $existingPayloads),
        "payload" => $result->data->payload,
      ];
    }

    return [
      "status" => $result->message,
      "payload" => null,
    ];
  }

  /**
   * The function checks if the characters in two strings match, allowing for certain exceptions.
   * 
   * @param str1 The first string to compare.
   * @param str2 The above code defines a function named `checkCharactersMatch` that takes two string
   * parameters, `` and ``. The function compares the characters of the two strings and returns
   * `true` if all the characters match or if any of the characters are 'x', '-', or ' '.
   * 
   * @return a boolean value. It returns true if all characters in the two input strings match or if they
   * are either 'x', '-', or ' '. It returns false if there is any character that does not match and is
   * not one of the allowed characters.
   */
  public function checkCharactersMatch($str1, $str2) {
    // Remove spaces and hyphens from both strings
    $str1 = str_replace([' ', '-', ';'], '', $str1);
    $str2 = str_replace([' ', '-', ';'], '', $str2);

    $str1 = mb_strtolower($str1);
    $str2 = mb_strtolower($str2);

    $length = min(mb_strlen($str1), mb_strlen($str2));

    for ($i = 0; $i < $length; $i++) {
      $char1 = mb_substr($str1, $i, 1);
      $char2 = mb_substr($str2, $i, 1);

      if ($char1 !== $char2 && $char1 !== 'x' && $char2 !== 'x') return false;
    }

    return true;
  }

  /**
   * The function "checkMatch" checks if the receiver's name and number match the target name and number.
   * 
   * @param data The `data` parameter is an array that contains information about the receiver. It has
   * the following structure:
   * @param targetNameTh The targetNameTh parameter is the target name in Thai language.
   * @param targetNameEn The parameter `targetNameEn` is the English name that we want to match with the
   * receiver's English name.
   * @param targetNumber The parameter `targetNumber` is the target bank account number that we want to
   * match with the receiver's bank account number.
   * 
   * @return a boolean value. It returns true if either the Thai or English names match the target name,
   * and the number matches the target number. Otherwise, it returns false.
   */
  public function checkMatch($data, $total, $targetBankAccountName, $targetBankNumber, $existingPayloads) {
    $receiverAmount = $data->data->amount->amount;
    $receiverNumber = $data->data->receiver->account->bank->account;

    $amountMatches = $receiverAmount == $total;
    $numberMatches = $this->checkCharactersMatch($targetBankNumber, $receiverNumber);
  
    if ($numberMatches && $amountMatches && !in_array($data->data->payload, $existingPayloads)) {
      return "verified";
    }

    if (!$numberMatches) {
      return "invalid_bank";
    }

    if (!$amountMatches) {
      return "invalid_amount";
    }

    if (in_array($data->data->payload, $existingPayloads)) {
      return "duplicated_slip";
    }

    return "invalid_slip";
  }

  /**
   * The function `getAbsoluteImagePath` in PHP retrieves the absolute file path from a given URL based
   * on WordPress upload and content directories.
   * 
   * @param url The `getAbsoluteImagePath` function you provided is a PHP function that takes a URL as
   * a parameter and attempts to convert it to an absolute file path based on WordPress upload and
   * content directories.
   * 
   * @return If the input URL matches the base URL for uploads or the content URL for WordPress, the
   * function will return the corresponding absolute path on the server. Otherwise, it will return
   * null.
   */
  public function getAbsoluteImagePath($url) {
    $upload_dir = wp_upload_dir();
    $base_url = $upload_dir['baseurl'];
    $base_dir = $upload_dir['basedir']; 

    if (strpos($url, $base_url) === 0) {
      return str_replace($base_url, $base_dir, $url);
    }

    $content_url = content_url(); 
    $content_dir = WP_CONTENT_DIR;

    if (strpos($url, $content_url) === 0) {
      return str_replace($content_url, $content_dir, $url);
    }

    return null;
  }
}
<?php 

namespace Repositories;

class OrderRepository {
  /**
   * The getSlip function retrieves the slip for a given order ID.
   * 
   * @param orderId The orderId parameter is the unique identifier for a specific order. It is used to
   * retrieve the slip for that order.
   */
  public function getMyOrders() {
    global $wpdb;

    $userId = get_current_user_id();

    $order_statuses = array('wc-pending', 'wc-processing');

    $orders = wc_get_orders(array(
      'customer' => $userId,
      'status'   => $order_statuses,
    ));

    return $orders;
  }

  /**
   * Change the status of a given order.
   *
   * @param int $orderId The ID of the order to update.
   * @param string $newStatus The new status to set for the order.
   */
  public function changeOrderStatus($orderId, $newStatus) {
    global $wpdb;

    $table_name = $wpdb->prefix . 'wc_orders';
    
    // Update the order status in the database
    $wpdb->update(
      $table_name,
      array('status' => $newStatus),
      array('id' => $orderId),
      array('%s'),
      array('%d')
    );
  }
}
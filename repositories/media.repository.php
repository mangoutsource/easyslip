<?php

namespace Repositories;

class MediaRepository {
 /**
  * The function uploads a file to the WordPress media library and returns the URL of the uploaded
  * file.
  * 
  * @param file The "file" parameter is the file that you want to upload. It should be an array
  * containing information about the file, such as its name, type, and temporary location.
  * 
  * @return the URL of the uploaded attachment.
  */
  public function upload($file) {
    if (!function_exists( 'wp_handle_upload' )) require_once ABSPATH . 'wp-admin/includes/file.php';

    $movefile = wp_handle_upload($file, ['test_form' => false]);
    $file_url = $movefile['url'];

    $attachment = array(
      'post_title' => sanitize_file_name(basename($file_url)),
      'post_content' => '',
      'post_status' => 'inherit',
      'post_mime_type' => $file['type'],
    );

    $attachment_id = wp_insert_attachment($attachment, $file_url);

    return wp_get_attachment_url($attachment_id);
  }
}
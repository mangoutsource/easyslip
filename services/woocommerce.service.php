<?php 

namespace Services;

class WoocommerceService {
  public function __construct() {
  }

  /**
   * The function "getBanks" retrieves BACS (Bankers' Automated Clearing Services) account information
   * from WooCommerce settings and returns an array of bank account objects.
   * 
   * @return an array of objects that represent bank accounts. Each object has properties for the
   * account name, account number, and bank name.
   */
  public function getBanks() {
    $accounts = array(); 
    $settings = get_option('woocommerce_bacs_accounts');

    foreach ($settings as $index => $account) {
      $accounts[] = (object) array(
        'id' => $index,
        'name' => isset($account['account_name']) ? esc_html($account['account_name']) : '',
        'number' => isset($account['account_number']) ? esc_html($account['account_number']) : '',
        'bank' => isset($account['bank_name']) ? esc_html($account['bank_name']) : '',
      );
    }

    return $accounts;
  }

 /**
  * The function "getBankById" retrieves bank information based on the provided ID.
  * 
  * @param id The parameter "id" is used to specify the ID of the bank account that you want to
  * retrieve information for.
  * 
  * @return an array with the following keys and values:
  * - 'id': the provided  parameter
  * - 'name': the value of the 'account_name' key from the  array, or an empty string if it is
  * not set
  * - 'number': the value of the 'account_number' key from the  array, or an empty string if it
  * is not set
  */
  public function getBankById($id) {
    $settings = get_option('woocommerce_bacs_accounts');
    $account = $settings[$id];

    $obj = new \StdClass();
    $obj->id = $id;
    $obj->name = isset($account['account_name']) ? esc_html($account['account_name']) : '';
    $obj->number = isset($account['account_number']) ? esc_html($account['account_number']) : '';
    $obj->bank = isset($account['bank_name']) ? esc_html($account['bank_name']) : '';

    return $obj;
  }

  /**
   * The function `getBankLogo` takes a bank name as input and returns the URL of the corresponding bank
   * logo image.
   * 
   * @param bank_name The parameter `bank_name` is a string that represents the name of a bank.
   * 
   * @return the URL of the bank logo image based on the given bank name.
   */
  public function getBankLogo($bank_name)
  {
      $bankLogos = array(
        'none'      => '',
        'kbank'     => 'กสิกรไทย, กสิกร, 开泰银行, kasikorn, kbank',
        'scb'       => 'ไทยพาณิชย์, 汇商银行, siam commercial, scb',
        'bbl'       => 'กรุงเทพ, กรุงเทพฯ, 盘古银行, bangkok, bualuang, bl',
        'ktb'       => 'กรุงไทย, 泰京银行, krungthai, ktb',
        'ttb'       => 'ทีเอ็มบีธนชาต, ทหารไทยธนชาต, ทีทีบี, thai military, ttb, ทหารไทย, 泰国军人银行, thai military, tmb',
        'bay'       => 'กรุงศรี, กรุงศรีฯ, กรุงศรีอยุธยา, 大城银行, bay, krungsri',
        'citi'      => 'ซิดี้, citi',
        'gsb'       => 'ออมสิน, 政府储蓄银行, gsb',
        'tbank'     => 'ธนชาต, 泰纳昌银行, tbank',
        'uob'       => 'ยูโอบี, 大华银行, uob',
        'isbt'      => 'อิสลาม, islamic, ibank, islamic bank of thailand',
        'ghb'       => 'อาคารสงเคราะห์, 住宅银行, ธอส, ghb, ธอส g h bank, g h bank ธอส',
        'baac'      => 'baa, ธกส, ธ.ก.ส., 农业合作银行, เพื่อการเกษตร, เพื่อการเกษตรและสหกรณ์การเกษตร',
        'tisco'     => 'tisco, 铁士古银行, ทิสโก้',
        'kkp'       => 'kkp, kiatnakin, 甲那金银行, เกียรตินาคิน',
        'icbc'      => 'ไอซีบีซี, 工商银行, icbc',
        'boc'       => 'แห่งประเทศจีน, 中国银行, bank of china, boc',
        'promptpay' => 'promptpay, พร้อมเพย์',
        'truemoney' => 'ทรู, true, truemoney',
        'cimb'      => 'ซีไอเอ็มบี, cimb, ซีไอเอ็มบีไทย, cimb ซีไอเอ็มบี, cimb ซีไอเอ็มบีไทย',
        'bca'       => 'bca',
        'bi'        => 'bi, bank indonesia',
        'bni'       => 'bni',
        'bri'       => 'bri',
        'mandiri'   => 'mandiri',
        'btpn'      => 'btpn',
        'bcel'      => 'การค้าต่างประเทศลาว, ການຄ້າຕ່າງປະເທດລາວ, bcel', 
        'aba'       => 'aba', 
        'wing'      => 'wing',
        'bofa'      => 'bank of america',
        'bnp'       => 'bnp paribas, bnp',
        'btn'       => 'btn, bank tabungan negara',
        'deutsche'  => 'deutsche, ดอย ซ์ แบงก์',
        'hsbc'      => 'hsbc',
        'jpmorgan'  => 'jpmorgan, jp morgan',
        'lh'        => 'lh, แลนด์ แอนด์ เฮาส์, lh bank, lh bank แลนด์ แอนด์ เฮ้าส์',
        'mizuho'    => 'mizuho',
        'sc'        => 'sc, สแตนดาร์ดชาร์เตอร์ด, standard chartered',
        'smbc'      => 'smbc, sumitomo mitsui banking corporation',
      );

      $normalizedBankName = strtolower(trim(str_replace(['ธนาคาร', '(', ')'], '', $bank_name)));
      $matchingLogos = array_keys(preg_grep('/' . $normalizedBankName . '/m', $bankLogos));
  
      $returnLogo = empty($matchingLogos) ? 'none' : $matchingLogos[0];
  
      return plugin_dir_url(__FILE__) . '../public/images/' . $returnLogo . '.webp';
  }
}
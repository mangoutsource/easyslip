<?php 

namespace Services;

use Repositories\OrderRepository;

class OrderService {
  private $orderRepository;

  public function __construct() {
    $this->orderRepository = new OrderRepository();
  }

  /**
   * The getSlip function retrieves the slip for a given order ID.
   * 
   * @param orderId The orderId parameter is the unique identifier for a specific order. It is used to
   * retrieve the slip for that order.
   */
  public function getMyOrders() {
    return $this->orderRepository->getMyOrders();
  }

  /**
   * Change the status of a given order.
   *
   * @param int $orderId The ID of the order to update.
   * @param string $newStatus The new status to set for the order.
   */
  public function changeOrderStatus($orderId, $newStatus) {
    return $this->orderRepository->changeOrderStatus($orderId, $newStatus);
  }
}
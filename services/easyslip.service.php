<?php 

namespace Services;

use Repositories\EasySlipRepository;
use Repositories\MediaRepository;
use Repositories\OrderRepository;
use Repositories\CarbonRepository;
use Repositories\WordPressRepository;
use Repositories\LineNotifyRepository;

class EasySlipService {
  private $easySlipRepository;
  private $mediaRepository;
  private $wordpressRepository;
  private $orderRepository;
  private $carbonRepository;
  private $lineNotifyRepository;

  public function __construct() {
    $this->easySlipRepository = new EasySlipRepository(); 
    $this->mediaRepository = new MediaRepository(); 
    $this->wordpressRepository = new WordPressRepository();
    $this->orderRepository = new OrderRepository();
    $this->carbonRepository = new CarbonRepository();
    $this->lineNotifyRepository = new LineNotifyRepository();
  }

  /**
   * The getSlip function retrieves the slip for a given order ID.
   * 
   * @param orderId The orderId parameter is the unique identifier for a specific order. It is used to
   * retrieve the slip for that order.
   */
  public function getSlip($orderId) {
    return $this->carbonRepository->getField($orderId, 'easyslip_image');
  }

  /**
   * The getStatus function retrieves the 'easyslip_status' meta value for a given order ID.
   * 
   * @param orderId The orderId parameter is the unique identifier for the order. It is used to retrieve
   * the status of a specific order.
   * 
   * @return the value of the 'easyslip_status' meta field for the given .
   */
  public function getStatus($orderId) {
    return $this->carbonRepository->getField($orderId, 'easyslip_status');
  }

  /**
   * The getLicense function returns the value of the 'easyslip_option' option from the
   * carbonRepository.
   * 
   * @return the value of the 'easyslip_option' option from the carbon repository.
   */
  public function getLicense() {
    return $this->carbonRepository->getOption('easyslip_license');
  }

 /**
  * The function "verifySlip" verifies a slip for a given order ID and updates the "easyslip_status"
  * field to "verified" using the Carbon repository.
  * 
  * @param orderId The orderId parameter is the unique identifier for the order that needs to be
  * verified.
  */
  public function verifySlip($orderId) {
    $order = wc_get_order($orderId);
    $this->carbonRepository->saveField($orderId, 'easyslip_status', "verified");
  }

  /**
   * The saveSlip function is used to save a slip image for a given order ID.
   * 
   * @param orderId The unique identifier for the order. It could be an integer or a string.
   * @param imageURL The URL of the slip image that needs to be saved.
   */
  public function checkSlip($orderId) {
    $order = wc_get_order($orderId);

    $image = $this->carbonRepository->getField($orderId, 'easyslip_image');
    $bankAccountName = $this->carbonRepository->getField($orderId, 'easyslip_bank_account_name');
    $bankAccountNumber = $this->carbonRepository->getField($orderId, 'easyslip_bank_account_number');

    $existingPayloads = $this->wordpressRepository->getPostMetaValuesFromMetaKeyWithoutPostId("_easyslip_payload", [$orderId]);

    $result = $this->easySlipRepository->verifyPayload($image, $order->get_total(), $bankAccountName, $bankAccountNumber, $existingPayloads);
    $this->carbonRepository->saveField($orderId, 'easyslip_status', $result['status']);
    $this->carbonRepository->saveField($orderId, 'easyslip_payload', $result['payload']);

    $this->lineNotifyRepository->sendOrderSlipVerificationNotification($order, $image);
  }

  /**
   * The saveSlip function is used to save a slip image for a given order ID.
   * 
   * @param orderId The unique identifier for the order. It could be an integer or a string.
   * @param imageURL The URL of the slip image that needs to be saved.
   */
  public function saveSlip($orderId, $file, $bankName, $bankAccountName, $bankAccountNumber) {
    $order = wc_get_order($orderId);

    $existingPayloads = $this->wordpressRepository->getPostMetaValuesFromMetaKeyWithoutPostId("_easyslip_payload", [$orderId]);

    $image = $this->mediaRepository->upload($file);
    $result = $this->easySlipRepository->verifyPayload($image, $order->get_total(), $bankAccountName, $bankAccountNumber, $existingPayloads);

    $this->carbonRepository->saveField($orderId, 'easyslip_image', $image);
    $this->carbonRepository->saveField($orderId, 'easyslip_bank_name', $bankName);
    $this->carbonRepository->saveField($orderId, 'easyslip_bank_account_name', $bankAccountName);
    $this->carbonRepository->saveField($orderId, 'easyslip_bank_account_number', $bankAccountNumber);
    $this->carbonRepository->saveField($orderId, 'easyslip_status', $result['status']);
    $this->carbonRepository->saveField($orderId, 'easyslip_payload', $result['payload']);

    $this->lineNotifyRepository->sendOrderSlipVerificationNotification($order, $image);

    $changeTo = $this->carbonRepository->getOption('easyslip_change_to');

    if (!$changeTo) $changeTo = 'wc-on-hold';
    $order->update_status( $changeTo );
  }

  /**
   * The function checks if the 'easyslip_image' option is set in the theme options and returns the
   * value.
   * 
   * @return the value of the 'easyslip_image' option from the theme's Carbon Fields settings.
   */
  public function shouldShowFormOnThankYouPage() {
    return $this->carbonRepository->getOption('easyslip_display_thankyou');
  }

  /**
   * The function `getBankImage` takes a bank name as input and returns the URL of the corresponding
   * bank image.
   * 
   * @param bankName The parameter `bankName` is the name of the bank for which you want to retrieve
   * the corresponding bank image.
   * 
   * @return the URL of the bank image based on the given bank name.
   */
  public function getBankImage($bankName)
  {
      $array = array(
          'none'      => '',
          'kbank'     => 'กสิกรไทย, กสิกร, 开泰银行, kasikorn, kbank',
          'scb'       => 'ไทยพาณิชย์, 汇商银行, siam commercial, scb',
          'bbl'       => 'กรุงเทพ, กรุงเทพฯ, 盘古银行, bangkok, bualuang, bl',
          'ktb'       => 'กรุงไทย, 泰京银行, krungthai, ktb',
          'ttb'       => 'ทีเอ็มบีธนชาต, ทหารไทยธนชาต, ทีทีบี, thai military, ttb, ทหารไทย, 泰国军人银行, thai military, tmb',
          'bay'       => 'กรุงศรี, กรุงศรีฯ, กรุงศรีอยุธยา, 大城银行, bay, krungsri',
          'citi'      => 'ซิดี้, citi',
          'gsb'       => 'ออมสิน, 政府储蓄银行, gsb',
          'tbank'     => 'ธนชาต, 泰纳昌银行, tbank',
          'uob'       => 'ยูโอบี, 大华银行, uob',
          'isbt'      => 'อิสลาม, islamic, ibank, islamic bank of thailand',
          'ghb'       => 'อาคารสงเคราะห์, 住宅银行, ธอส, ghb, ธอส g h bank, g h bank ธอส',
          'baac'      => 'baa, ธกส, ธ.ก.ส., 农业合作银行, เพื่อการเกษตร, เพื่อการเกษตรและสหกรณ์การเกษตร',
          'tisco'     => 'tisco, 铁士古银行, ทิสโก้',
          'kkp'       => 'kkp, kiatnakin, 甲那金银行, เกียรตินาคิน',
          'icbc'      => 'ไอซีบีซี, 工商银行, icbc',
          'boc'       => 'แห่งประเทศจีน, 中国银行, bank of china, boc',
          'promptpay' => 'promptpay, พร้อมเพย์',
          'truemoney' => 'ทรู, true, truemoney',
          'cimb'      => 'ซีไอเอ็มบี, cimb, ซีไอเอ็มบีไทย, cimb ซีไอเอ็มบี, cimb ซีไอเอ็มบีไทย',
          'bca'       => 'bca', // INDONESIA BANKS
          'bi'        => 'bi, bank indonesia',
          'bni'       => 'bni',
          'bri'       => 'bri',
          'mandiri'   => 'mandiri',
          'btpn'      => 'btpn',
          'bcel'      => 'การค้าต่างประเทศลาว, ການຄ້າຕ່າງປະເທດລາວ, bcel', // LOAS BANKS
          'aba'       => 'aba', // CAMOBDIA BANKS
          'wing'      => 'wing',
          'bofa'      => 'bank of america',
          'bnp'       => 'bnp paribas, bnp',
          'btn'       => 'btn, bank tabungan negara',
          'deutsche'  => 'deutsche, ดอย ซ์ แบงก์',
          'hsbc'      => 'hsbc',
          'jpmorgan'  => 'jpmorgan, jp morgan',
          'lh'        => 'lh, แลนด์ แอนด์ เฮาส์, lh bank, lh bank แลนด์ แอนด์ เฮ้าส์',
          'mizuho'    => 'mizuho',
          'sc'        => 'sc, สแตนดาร์ดชาร์เตอร์ด, standard chartered',
          'smbc'      => 'smbc, sumitomo mitsui banking corporation',
      );
      $bank_name = trim(str_replace('ธนาคาร', '', $bank_name));
      $bank_name = trim(str_replace('(', '', $bank_name));
      $bank_name = trim(str_replace(')', '', $bank_name));
      $str = strtolower($bank_name);
      $find = preg_grep('/'.$str.'/m', $array);

      if(empty($find)) {
        $return_logo = 'none';
      } else {
        $key = array_keys($find);
        $return_logo = $key[0];
    }

    return plugin_dir_url(__FILE__) . 'public/img/' . $return_logo . '.webp';
  }
}
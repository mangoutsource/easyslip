<?php 

namespace Clients;

use Services\OrderService;
use Services\EasySlipService;
use Services\WoocommerceService;

class ShortcodeClient {
  private $orderService;
  private $easySlipService;
  private $woocommerceService;

  /**
   * The function registers Carbon Fields in WordPress.
   */
  public function __construct() {
    $this->orderService = new OrderService();
    $this->easySlipService = new EasySlipService();
    $this->woocommerceService = new WoocommerceService();

    add_shortcode('easyslip', [$this, 'generate_easyslip_shortcode']);
    add_action('woocommerce_thankyou_bacs', [$this, 'add_shortcode_after_order_details'], 5, 1);
  }

  /**
   * The function adds a shortcode after the order details in PHP.
   */
  public function add_shortcode_after_order_details($orderId) {
    $order = wc_get_order($orderId);


    if ($order->get_payment_method() !== 'bacs') {
      return;
    }

    if ($this->easySlipService->shouldShowFormOnThankYouPage() == false) {
      return;
    }

    echo do_shortcode('[easyslip id="'. $orderId .'"]');
  }

  /**
   * The function returns a greeting message for a website.
   * 
   * @return The string 'Hello, welcome to my website!' is being returned.
   */
  public function generate_easyslip_shortcode($atts) {
    if (!$this->easySlipService->getLicense()) {
      return;
    }
    
    ob_start(); // Start output buffering to capture HTML output

    $defaultOrderId = isset($atts['id']) ? $atts['id'] : null;
    $orders = $this->orderService ->getMyOrders();
    $banks = $this->woocommerceService->getBanks();

    $success = false;

    if ($_POST && $_FILES) {
      $id = $_POST['order_id'];
      $bankId = $_POST['bank_id'];
      $file = $_FILES['order_slip'];

      $bank = $this->woocommerceService->getBankById($bankId);

      $this->easySlipService->saveSlip($id, $file, $bank->bank, $bank->name, $bank->number);
      $success = true;
    }
    ?>
    <style>
      :root {
        --es-bg-error: #e56a6a;
        --es-bg-success: #57ad68;
        --es-accent: var(--s-color-1, var(--primary-color, #004999));
        --es-bg: #fff;
        --es-bg-disabled: #f5f5f7;
        --es-border: #c5c5c7;
        --es-text: #333;
        --es-text-error: #c00;
        --es-text-success: #57ad68;
        --es-round: 3px;
      }

      .es-success {
        background-color: var(--es-bg-success);
        margin-bottom: 20px;
      }

      .es-form {
        border: 1px solid var(--es-border);
        padding: 20px 30px;
      }

      .es-form * {
        box-sizing: border-box;
        line-height: 1.6;
      }

      .es-input {
        border: 1px solid var(--es-border);
        background-color: var(--es-bg);
        font-size: 16px;
        padding: 4px 6px 5px;
        width: 100%;
        min-height: 38px;
        border-radius: var(--es-round);
      }

      .es-button {
        background-color: var(--es-accent);
        color: #fff;
        padding: 0.6em 2em;
        border: none;
        font-size: 1.125em;
        border-radius: var(--es-round);
        margin-bottom: 6px;
        line-height: 1.3;
        cursor: pointer;
      }

      .es-form label {
        margin-bottom: 5px;
      }

      .es-form label span:only-child {
        color: var(--es-text-error);
        margin-left: 5px;
      }

      .es-col {
        display: flex;
        flex-direction: column;
        margin-bottom: 20px;
      }

      .es-bank-container {
        display: flex;
        flex-direction: column;
        gap: 3px;
        margin-bottom: 20px;
      }

      .es-bank-row {
        display: flex;
        align-items: center;
        gap: 20px;
        border: 2px solid var(--es-border);
        border-radius: var(--es-round);
        padding: 10px;
      }

      .es-bank-row.active {
        border-color: #004999; 
      }

      .es-bank-info {
        display: flex;
        flex-direction: column;
      }

      .es-bank-info span {
        line-height: 1.4;
      }

      .es-bank-info-row span {
        display: inline-block;
        min-width: 140px;
      }

      @media screen and (max-width: 480px) {
        .es-bank-row img {
          max-width: 50px;
        }

        span.es-bank-name-col {
          display: none;
        }

        .es-bank-row {
          gap: 10px;
        }
      }
    </style>

    <?php if ($success): ?>
      <div class="es-form es-success">
        <?php _e('Confirm payment successfully', 'easyslip-main'); ?>
      </div>
    <?php endif; ?>

    <form id="easyslip-form" class="es-form" action="" method="post" enctype="multipart/form-data">
      <?php if ($defaultOrderId): ?>
        <input type="hidden" name="order_id" value="<?php echo $defaultOrderId; ?>" required>
      <?php elseif (count($orders) > 0): ?>
        <div class="es-col">
          <label for="order_id"><?php _e('Order No.', 'easyslip-main'); ?><span>*</span></label>
          <select name="order_id" class="es-input">
            <?php foreach ($orders as $order): ?>
              <option value="<?php echo $order->ID; ?>"><?php echo sprintf( __('No. %s. payment total: %s', 'easyslip-main'), $order->get_id(), wc_price($order->get_total())); ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      <?php else: ?>
        <div class="es-col">
          <label for="order_id"><?php _e('Order No.', 'easyslip-main'); ?><span>*</span></label>
          <input type="text" name="order_id" class="es-input" required>
        </div>
      <?php endif; ?>
      <div class="es-bank-container radio-group">
        <label for="bank_id"><?php _e('Bank Transfer', 'easyslip-main'); ?><span>*</span></label>
        <?php foreach ($banks as $bank): ?>
          <label class="es-bank-row">
            <input type="radio" name="bank_id" value="<?php echo $bank->id; ?>" required />
            <img src="<?php echo $this->woocommerceService->getBankLogo($bank->bank); ?>" width="70" />
            <div class="es-bank-info">
              <div class="es-bank-info-row">
                <span class="es-bank-nam es-bank-name-col"><?php _e('Bank Name', 'easyslip-main'); ?></span>
                <span class="es-bank-name"><?php echo $bank->bank; ?></span>
              </div>
              <div class="es-bank-info-row">
                <span class="es-bank-name es-bank-name-col"><?php _e('Account No.', 'easyslip-main'); ?></span>
                <span class="es-bank-name"><?php echo $bank->number; ?></span>
              </div>
              <div class="es-bank-info-row">
                <span class="es-bank-name es-bank-name-col"><?php _e('Account Name', 'easyslip-main'); ?></span>
                <span class="es-bank-name"><?php echo $bank->name; ?></span>
              </div>
            </div>
          </label>
        <?php endforeach; ?>
      </div>
      <div class="es-col">
        <label for="order_slip"><?php _e('Payment Slip', 'easyslip-main'); ?><span>*</span></label>
        <input type="file" name="order_slip" id="order_slip" class="es-input" accept="image/jpeg, image/png" required>
      </div>
      <?php wp_nonce_field('order_form_nonce', 'order_form_nonce'); ?>
      <button type="submit" class="es-button"><?php _e('Confirm Payment', 'easyslip-main'); ?></button>
    </form>

    <script>
      var esBankRows = document.querySelectorAll('.es-bank-row');

      esBankRows.forEach(function (esBankRow) {
        var radioInput = esBankRow.querySelector('input[type="radio"]');

        radioInput.addEventListener('change', function () {
          esBankRows.forEach(function (row) {
            row.classList.remove('active');
          });

          if (radioInput.checked) {
            esBankRow.classList.add('active');
          }
        });
      });
    </script>

    <?php
    return ob_get_clean();
  }
}
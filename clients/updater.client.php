<?php 

namespace Clients;

class UpdaterClient {
  public $plugin_slug;
  public $version;
  public $cache_key;
  public $cache_allowed;

  public function __construct() {
    $this->plugin_slug = EASYSLIP_SLUG;
    $this->version = EASYSLIP_VERSION;
    $this->cache_key = EASYSLIP_SLUG;
    $this->cache_allowed = true;

    add_filter( 'plugins_api', array( $this, 'info' ), 20, 3 );
    add_filter( 'site_transient_update_plugins', array( $this, 'update' ) );
    add_action( 'upgrader_process_complete', array( $this, 'purge' ), 10, 2 );
  }

  /**
   * The function retrieves data from a remote URL and caches it for a day if the cache is allowed.
   * 
   * @return the JSON response from the remote URL.
   */
  public function request(){
    $remote = get_transient( $this->cache_key );

    if( false === $remote || ! $this->cache_allowed ) {
      $remote = wp_remote_get('https://gitlab.com/mangoutsource/easyslip/-/raw/main/info.json');

      if(is_wp_error( $remote ) || 200 !== wp_remote_retrieve_response_code( $remote ) || empty( wp_remote_retrieve_body( $remote ) )) {
        return false;
      }

      set_transient($this->cache_key, $remote, DAY_IN_SECONDS );
    }

    $remote = json_decode( wp_remote_retrieve_body( $remote ) );
    return $remote;
  }

  /**
   * The function retrieves plugin information and returns it in a standardized format.
   * 
   * @param res The `` parameter is the default response object that is passed to the `info`
   * function. It contains information about the plugin, such as its name, version, author, etc.
   * @param action The "action" parameter is a string that specifies the type of action being performed.
   * In this case, it is checking if the action is "plugin_information".
   * @param args The "args" parameter is an object that contains information about the plugin being
   * requested. It typically includes the plugin slug, which is used to identify the plugin uniquely.
   * 
   * @return an object with various properties such as name, slug, version, tested, requires, author,
   * author_profile, download_link, trunk, requires_php, last_updated, sections, and banners.
   */
  function info( $res, $action, $args ) {
    if( 'plugin_information' !== $action ) {
      return $res;
    }

    if( $this->plugin_slug !== $args->slug ) {
      return $res;
    }

    $remote = $this->request();

    if( ! $remote ) {
      return $res;
    }

    $res = new \stdClass();

    $res->name = $remote->name;
    $res->slug = $remote->slug;
    $res->version = $remote->version;
    $res->tested = $remote->tested;
    $res->requires = $remote->requires;
    $res->author = $remote->author;
    $res->author_profile = $remote->author_profile;
    $res->download_link = $remote->download_url;
    $res->trunk = $remote->download_url;
    $res->requires_php = $remote->requires_php;
    $res->last_updated = $remote->last_updated;

    $res->sections = array(
      'description' => $remote->sections->description,
      'installation' => $remote->sections->installation,
      'changelog' => $remote->sections->changelog
    );

    if( ! empty( $remote->banners ) ) {
      $res->banners = array(
        'low' => $remote->banners->low,
        'high' => $remote->banners->high
      );
    }

    return $res;

  }

  /**
   * The function checks if there is a newer version of a plugin available and adds it to the update
   * transient if it meets certain conditions.
   * 
   * @param transient The `` parameter is an object that contains information about the current
   * state of the plugin/theme update. It includes properties like `checked` (whether the plugin/theme
   * has been checked for updates), `response` (an array of plugins/themes that have updates available),
   * and more.
   * 
   * @return the updated transient object.
   */
  public function update( $transient ) {
    if (empty($transient->checked)) {
      return $transient;
    }

    $remote = $this->request();

    if(
      $remote
      && version_compare( $this->version, $remote->version, '<' )
      && version_compare( $remote->requires, get_bloginfo( 'version' ), '<=' )
      && version_compare( $remote->requires_php, PHP_VERSION, '<' )
    ) {
      $res = new \stdClass();
      $res->slug = $this->plugin_slug;
      $res->plugin = $this->plugin_slug . '/index.php';
      $res->new_version = $remote->version;
      $res->tested = $remote->tested;
      $res->package = $remote->download_url;

      $transient->response[ $res->plugin ] = $res;
    }

    return $transient;
  }

  /**
   * The function purges a cache transient if caching is allowed and the action is an update for a
   * plugin.
   * 
   * @param upgrader The  parameter is an object that handles the upgrading process. It is
   * responsible for installing, updating, and deleting plugins, themes, and WordPress core files.
   * @param options The "options" parameter is an array that contains information about the upgrade
   * action being performed. It includes the following keys:
   */
  public function purge( $upgrader, $options ){
    if ($this->cache_allowed && 'update' === $options['action'] && 'plugin' === $options[ 'type' ]) {
      delete_transient( $this->cache_key );
    }
  }
}
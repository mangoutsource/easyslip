<?php 

namespace Clients;

use Services\EasySlipService;
use Helpers\URLHelper;

class HookClient {
  /* The line `private ;` is declaring a private property called `` in
  the `HookClient` class. This property is used to store an instance of the `EasySlipService` class,
  which is a service class used for retrieving slip information and status. By declaring it as
  private, it can only be accessed within the `HookClient` class. */
  private $easySlipService;

  /**
   * The above function is a constructor that initializes the EasySlipService and adds a custom text
   * under the shipping address in the WooCommerce admin order data.
   */
  public function __construct() {
    $this->easySlipService = new EasySlipService();    

    add_action( 'woocommerce_admin_order_data_after_shipping_address', [$this, 'custom_text_under_shipping'], 10, 1 );
    add_filter('manage_woocommerce_page_wc-orders_columns', [$this, 'custom_order_column_heading'], 99);
    add_filter('manage_edit-shop_order_columns', [$this, 'custom_order_column_heading'], 99);
    add_action('manage_woocommerce_page_wc-orders_custom_column', [$this, 'customer_order_column_value'], 5, 2);
    add_action('manage_shop_order_posts_custom_column', [$this, 'customer_old_order_column_value']);
    add_action('init', [$this, 'check_slip'], 100);
  }

  /**
   * The function `custom_text_under_shipping` displays an image, status, and a button for retrying a
   * slip check.
   * 
   * @param order The "order" parameter is an object that represents an order. It likely contains
   * information such as the order ID, customer details, products, and other relevant information
   * related to the order.
   */
  public function custom_text_under_shipping($post_id) {
    $order = wc_get_order($post_id);
    $orderId = $order->get_id();

    $image = $this->easySlipService->getSlip($orderId);
    $status = $this->easySlipService->getStatus($orderId);

    $this->render_easy_slip($orderId, $image, $status);
  }

 /**
  * The function adds a new column called "EasySlip" to the existing columns in a table.
  * 
  * @param columns An array of columns that are currently being displayed in a table or list. Each
  * column is represented by a key-value pair, where the key is the column identifier and the value is
  * the column label or name.
  * 
  * @return an array of columns, with an additional column 'easyslip' added if the key 'order_status'
  * is found in the original columns array.
  */
  public function custom_order_column_heading($columns) {
    $new_columns = [];

    foreach($columns as $key => $column) {
        $new_columns[$key] = $columns[$key];

        if ($key !== 'order_status') continue;
        
        if ($this->easySlipService->getLicense()) {
          $new_columns['easyslip'] = __('EasySlip', 'easyslip-main');
        }
    }

    return $new_columns;
  }

  /**
   * The function "customer_order_column_value" checks if the column is "easyslip" and if so, it echoes
   * a hyphen ("-").
   * 
   * @param column The column parameter is a string that represents the name of the column in the
   * customer order table.
   * @param post_id The post_id parameter is the ID of the post or customer order for which you want to
   * retrieve the column value.
   */
  function customer_order_column_value($column, $post_id) {
    if ($column === 'easyslip' && $this->easySlipService->getLicense()) {
      $order = wc_get_order($post_id);
      $orderId = $order->get_id();

      $image = $this->easySlipService->getSlip($orderId);
      $status = $this->easySlipService->getStatus($orderId);

      $this->render_easy_slip($orderId, $image, $status);
    }
  }

  /**
   * The function "customer_order_column_value" checks if the column is "easyslip" and if so, it echoes
   * a hyphen ("-").
   * 
   * @param column The column parameter is a string that represents the name of the column in the
   * customer order table.
   * @param post_id The post_id parameter is the ID of the post or customer order for which you want to
   * retrieve the column value.
   */
  function customer_old_order_column_value($column) {
    if ($column === 'easyslip' && $this->easySlipService->getLicense()) {
      global $post;

      $order = wc_get_order($post->ID);
      $orderId = $order->get_id();

      $image = $this->easySlipService->getSlip($orderId);
      $status = $this->easySlipService->getStatus($orderId);

      $this->render_easy_slip($orderId, $image, $status);
    }
  }

  /**
   * The function `render_easy_slip` renders an HTML element containing an image, status text, and a
   * button.
   * 
   * @param image The "image" parameter is the URL of an image file that will be displayed in the
   * rendered output. It is used to show a slip image.
   * @param status The "status" parameter is a string that represents the status of the slip. It can have
   * one of the following values: "VERIFIED", "FAILED", or "PENDING".
   * 
   * @return The function does not explicitly return a value.
   */
  public function render_easy_slip($orderId, $image, $status) {
    if (!$image) {
      echo '-';
      return;
    }

    if ($status === 'verified') {
      $statusText = '<b style="color: green">'. __('Valid', 'easyslip-main') .'</b>';
      $error = null;
    } 

    if ($status !== 'verified') {
      $statusText = '<b style="color: red">'. __('Invalid', 'easyslip-main') .'</b>';
      $error = str_replace('_', ' ', ucwords($status));
    }

    $recheckURL = URLHelper::addQuery('slipcheck', $orderId);
    $verifiedURL = URLHelper::addQuery('slipverified', $orderId);

    echo '<style>.post-type-shop_order .wp-list-table td, .post-type-shop_order .wp-list-table th, .woocommerce_page_wc-orders .wp-list-table td, .woocommerce_page_wc-orders .wp-list-table th { width: 25ch; }</style>';

    echo '<div style="display: inline-flex; flex-direction: row; align-items: center; gap: 15px; min-width: 250px;">';
    echo '<a href="'. $image .'" target="_blank" style="display: inline-block;">';
    echo '<img src="'. $image .'" style="width: 50px; max-width: 100%; vertical-align: middle;" />';
    echo '</a>';
    echo '<div style="display: flex; flex-direction: column; gap: 10px;">';
    echo '<span style="line-height: 1">'. __('status: ', 'easyslip-main') . $statusText . '</span>';
    echo $error ? '<span style="line-height: 1; color: grey">'. $error . '</span>' : '';
    echo '<div style="display: flex; gap: 10px;">';
    echo '<a href="'. $verifiedURL .'">';
    echo '<button type="button" class="button easyslip-retry-action" style="text-decoration: none">'. __('Mark as verified', 'easyslip-main') .'</button>';
    echo '</a>';
    echo '<a href="'. $recheckURL .'">';
    echo '<button type="button" class="button easyslip-retry-action">'. __('Recheck', 'easyslip-main') .'</button>';
    echo '</a>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '<br>';
  }

  /**
   * The function "check_slip" is a PHP function that has not been implemented yet.
   */
  public function check_slip() {
    if (!$this->easySlipService->getLicense()) return;

    if (!URLHelper::contains('wc-orders') && !URLHelper::contains('shop_order')) return;

    if (URLHelper::contains('slipcheck')) {
      $orderId = URLHelper::getQuery('slipcheck');
      $this->easySlipService->checkSlip(URLHelper::getQuery('slipcheck'));

      wp_redirect(URLHelper::removeQuery('slipcheck'));
      exit();
    }

    if (URLHelper::contains('slipverified')) {
      $orderId = URLHelper::getQuery('slipverified');
      $this->easySlipService->verifySlip(URLHelper::getQuery('slipverified'));

      wp_redirect(URLHelper::removeQuery('slipverified'));
      exit();
    }
  }
}
<?php 

namespace Clients;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Carbon_Fields as CarbonFields;

class AdminClient {
  /**
   * The function registers Carbon Fields in WordPress.
   */
  public function __construct() {
    add_action('carbon_fields_register_fields', array($this, 'register_carbon_fields'));
    add_action('carbon_fields_register_fields', array($this, 'register_order_customer_fields'));
    add_action('plugins_loaded', [$this, 'init']);
  }

  /**
   * The init function initializes the CarbonFields library in PHP.
   */
  public function init() {
    CarbonFields::boot();
  }

  /**
   * The function "register_carbon_fields" registers a text field for the "License" option under the
   * "License: EasySlip" theme options container, with the parent page set to "options-general.php".
   */
  public function register_carbon_fields() {
    $order_statuses = wc_get_order_statuses();
    $statuses = [];

    foreach ($order_statuses as $status_slug => $status_label) {
      $statuses[$status_slug] = $status_label;
    }

    $fields = [
      Field::make('text', 'easyslip_license', __('License', 'easyslip-main')),
      Field::make('select', 'easyslip_change_to', __('After submission, change order status to', 'easyslip-main'))->set_options($statuses),
      Field::make('checkbox', 'easyslip_display_thankyou', __('Show form on Thank you page. shortcode: [easyslip id="<order id>"] where id is optional', 'easyslip-main')),
      Field::make('text', 'easyslip_line_token', __('Line notify token', 'easyslip-main')),
    ];

    Container::make('theme_options', __('EasySlip', 'easyslip-main'))->add_fields($fields)->set_page_parent('options-general.php');
  }

  public function register_order_customer_fields() {
    $fields = [
      Field::make('text', 'easyslip_image', __('EasySlip Image', 'easyslip-main')),
      Field::make('text', 'easyslip_status', __('EasySlip Status', 'easyslip-main')),
      Field::make('text', 'easyslip_payload', __('EasySlip Payload', 'easyslip-main')),
      Field::make('text', 'easyslip_bank_name', __('EasySlip Bank Name', 'easyslip-main')),
      Field::make('text', 'easyslip_bank_account_name', __('EasySlip Bank Account Name', 'easyslip-main')),
      Field::make('text', 'easyslip_bank_account_number', __('EasySlip Bank Account Number', 'easyslip-main')),
    ];

    Container::make('post_meta', __('EasySlip', 'easyslip-main'))->where('post_type', '=', 'order')->add_fields($fields);
  }
}